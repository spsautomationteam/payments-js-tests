@core @cc
Feature: Card processing without using the UI

  Scenario Outline: Normal Card Submission
    Given I have started a headless chrome browser
    And I have opened the rendered the payment template in the browser
    And I set the core.cc.card_number_input element to the configured value cc.cards.<card_type>
    And I set the core.cc.expiration_field element to the configured value cc.exp_month
    And I add the configured cc.exp_year value to the core.cc.expiration_field text field
    And I set the core.cc.cvv_input element to the configured value cc.cvv
    When I click on the core.payment_submit_button element
    And I wait until the core.result_div element is present
    Then the core.cc.approved_text element should exist

    Examples:
      | card_type        |
      | visa             |
      | mastercard       |
      | discover         |
      | american_express |