require 'watir'
require_relative './lib/config'
require_relative './lib/file_util'
require_relative './lib/template_util'
require_relative './lib/ui_element'
require_relative './lib/ui_elements'

Cucumber.logger.info("Testing with profile '#{Config.profile_name}'\n")
Cucumber.logger.level = Logger::DEBUG if Config['debug']

Watir.default_timeout = Config['watir_default_timeout'].to_i

World(
  Config,
  FileUtils,
  TemplateUtils,
  UIElements
)
