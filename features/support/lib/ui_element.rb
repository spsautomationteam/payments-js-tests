class UiElement
  attr_accessor :type, :selectors

  def initialize(type, selectors)
    @type      = type
    @selectors = Hash[selectors.map {|k, v| [k.respond_to?(:to_sym) ? k.to_sym : k, v]}]
  end
end