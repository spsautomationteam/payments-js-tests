require 'mustache'
require_relative 'config'

# Various utility functions related to mustache templates used for testing.
module TemplateUtils

  # Renders a mustache template file
  #
  # @param template_file_name [String] The name of the file to render without the file extension.
  # @param context [Hash] The context to be passed when rendering the template.
  # @return [String] The rendered mustache template.
  def self.render_template_file(template_file_name, context = {})
    raise 'Unable to find the config value "dirs.templates" in the provided config' if Config['dirs'].nil? || Config['dirs']['templates'].nil? # rubocop:disable Metrics/LineLength
    raise 'Unable to find the config value "payments_js" in the provided config' if Config['payments_js'].nil? # rubocop:disable Metrics/LineLength

    base_context = { :payments_js => Config['payments_js'] }
    Mustache.render_file(File.join(Config['dirs']['templates'], template_file_name), base_context.merge(context))
  end

  # Renders a mustache template file and opens it in a browser
  #
  # @param browser [Watir::Browser] The browser instance to open the template in.
  # @param (see render_template_file)
  def self.open_template_file_in_browser(browser, template_file_name, context = {})
    html = TemplateUtils.render_template_file(template_file_name, context)
    Cucumber.logger.info(html.empty?)
    html_file = Tempfile.new("#{template_file_name}_html")
    html_file.write(html)
    html_file.close
    browser.goto("file://#{html_file.path.tr('\\', '/')}")
    browser.wait
    html_file.delete
  end

end