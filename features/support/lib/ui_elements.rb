require 'yaml'
require_relative './ui_element'

module UIElements

  @dict = YAML.load(File.read('ui_elements.yaml'))

  def self.[](key)
    @dict[key]
  end

  def self.[]=(key, value)
    @dict[key] = value
  end

  def self.get(key)
    value = @dict.clone
    key.split('.').each do |k|
      break if value.nil?
      value = value[k]
    end
    UiElement.new(value['type'], value['selectors'])
  end

  def self.get_browser_element(browser, key)
    return nil if browser.nil?
    element_entry = UIElements.get(key)
    element       = case element_entry.type
                    when 'checkbox'
                      browser.checkbox(**element_entry.selectors)
                    else
                      browser.element(:tag_name => element_entry.type, **element_entry.selectors)
                    end
    element.to_subtype
  end


end