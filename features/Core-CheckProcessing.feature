@core @ach
Feature: Check processing without using the UI

  Scenario: Normal Check Submission
    Given I have started a headless chrome browser
    And I have opened the rendered the coreach template in the browser
    And I set the core.ach.routing_number element to the configured value ach.routing_number
    And I set the core.ach.account_number element to the configured value ach.account_number
    And I set the core.ach.account_type element to the configured value ach.account_type
    When I click on the core.payment_submit_button element
    And I wait until the core.result_div element is present
    Then the core.ach.approved_text element should exist