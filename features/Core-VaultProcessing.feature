@core @vault
Feature: Vault processing without using the UI

  Scenario: Normal Vault Submission
    Given I have started a headless chrome browser
    And I have opened the rendered the vault template in the browser
    And I set the core.vault.token_field element to the configured value vault.token.valid
    And I set the core.vault.token_cvv element to the configured value vault.cvv
    When I click on the core.payment_submit_button element
    And I wait until the core.result_div element is present
    Then the core.cc.approved_text element should exist

  Scenario: Normal Invalid Vault Submission
    Given I have started a headless chrome browser
    And I have opened the rendered the vault_invalid template in the browser
    And I set the core.vault.token_field element to the configured value vault.token.invalid
    And I set the core.vault.token_cvv element to the configured value vault.cvv
    When I click on the core.payment_submit_button element
    And I wait until the core.result_div element is present
    Then the core.cc.declined_text element should exist