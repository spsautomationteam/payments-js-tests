@ui @modal @cc
Feature: Check Processing using the UI and Modal

  Scenario Outline: CC Modal Regular Card Submission
    Given I have started a headless chrome browser
    And I have opened the rendered the payui template in the browser
    And I click on the show_cc_ui_button element
    And I set the sps.cc.card_number_input element to the configured value cc.cards.<card_type>
    And I set the sps.cc.expiration_month_select element to the configured value cc.exp_month
    And I set the sps.cc.expiration_year_select element to the configured value cc.exp_year
    And I set the sps.cc.cvv_input element to the configured value cc.cvv
    When I click on the sps.payment_submit_button element
    And I wait until the sps.result_div element is present
    Then the sps.approved_text element should exist

    Examples:
      | card_type        |
      | visa             |
      | mastercard       |
      | discover         |
      | american_express |

  Scenario Outline: CC Modal missing text fields
    Given I have started a headless chrome browser
    And I have opened the rendered the payui template in the browser
    And I click on the show_cc_ui_button element
    And I set the sps.cc.card_number_input element to the configured value cc.cards.visa
    And I set the sps.cc.expiration_month_select element to the configured value cc.exp_month
    And I set the sps.cc.expiration_year_select element to the configured value cc.exp_year
    And I set the sps.cc.cvv_input element to the configured value cc.cvv
    And I clear the <cleared_field> element
    Then the sps.payment_submit_button element should not be enabled

    Examples:
      | cleared_field            |
      | sps.cc.card_number_input |
      | sps.cc.cvv_input    |

  Scenario Outline: CC Modal missing select fields
    Given I have started a headless chrome browser
    And I have opened the rendered the payui template in the browser
    And I click on the show_cc_ui_button element
    And I set the sps.cc.card_number_input element to the configured value cc.cards.visa
    And I set the <filled_select> element to the configured value <select_value>
    And I set the sps.cc.cvv_input element to the configured value cc.cvv
    Then the sps.payment_submit_button element should not be enabled

    Examples:
      | filled_select                  | select_value |
      | sps.cc.expiration_month_select | cc.exp_month |
      | sps.cc.expiration_year_select  | cc.exp_year  |