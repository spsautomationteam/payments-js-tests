Given(/^I click on the (.+) element$/) do |element_name|
  UIElements.get_browser_element(@browser, element_name).fire_event :click
end

Given(/^I set the (.+) element to the configured value (\S+)$/) do |element_name, config_key|
  element      = UIElements.get(element_name)
  browser_elem = UIElements.get_browser_element(@browser, element_name)
  value        = Config.get(config_key)
  case element.type
  when 'select'
    browser_elem.select(value)
  else
    browser_elem.set(value)
  end
end

Given(/^I add the configured (\S+) value to the (.+) text field$/) do |config_key, element_name|
  # element      = UIElements.get(element_name)
  browser_elem = UIElements.get_browser_element(@browser, element_name)
  value        = Config.get(config_key)
  if browser_elem.is_a?(Watir::TextField)
    browser_elem.append(value)
  end
end

Given(/^I clear the (.+) element$/) do |element_name|
  element      = UIElements.get(element_name)
  browser_elem = UIElements.get_browser_element(@browser, element_name)
  case element.type
  when 'select'
    if browser_elem.multiple?
      browser_elem.clear
    else
      browser_elem.select(nil)
    end
  when 'input'
    if browser_elem.type == 'text' || browser_elem.type == 'tel'
      browser_elem.send_keys([:control, 'a'], :backspace)
    else
      browser_elem.clear
    end
  else
    browser_elem.clear
  end
end

When(/^I wait (\d+) seconds?$/) do |wait_time|
  sleep(wait_time)
end

When(/^I wait until the (.+) element is present$/) do |element_name|
  ui_element = UIElements.get(element_name)
  @browser.element(:tag_name => ui_element.type, **ui_element.selectors).wait_until_present
end

When(/^I wait while the (.+) element is present$/) do |element_name|
  ui_element = UIElements.get(element_name)
  @browser.element(:tag_name => ui_element.type, **ui_element.selectors).wait_while_present
end

Then(/^the (.+) element should exist$/) do |element_name|
  expect(UIElements.get_browser_element(@browser, element_name).exists?).to be_truthy
end

Then(/^the (.+) element should( not)? be enabled$/) do |element_name, not_found|
  expect(UIElements.get_browser_element(@browser, element_name).disabled?).to be !not_found.nil?
end