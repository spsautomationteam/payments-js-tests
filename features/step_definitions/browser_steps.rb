require 'watir'
require 'uri'

After do
  @browser.close unless @browser.nil?
end

Given(/^I have started a( headless)? (.*) browser$/) do |headless_found, browser_name|
  browser_sym = browser_name.tr(' ', '_').downcase.to_sym
  @browser = Watir::Browser.new(browser_sym, headless: !headless_found.nil?)
end

Given(/^I go to the url (.*)$/) do |url|
  @browser.goto(url)
end

Given(/^I go to the (.*) page$/) do |page_name|
  url = URI::join(Config['base_url'], Config['pages'][page_name.tr(' ', '_').downcase])
  step("I go to the url #{url}")
end

Then(/^the browser's url should be (.*)$/) do |url|
  expect(@browser.url).to eq(url)
end

Given(/^I have opened the rendered the (.*) template in the browser$/) do |template_name|
  TemplateUtils.open_template_file_in_browser(@browser, template_name)
end