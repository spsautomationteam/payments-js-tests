@ui @modal @ach
Feature: Check Processing using the UI and Modal

  Scenario: ACH Modal Regular Check Submission
    Given I have started a headless chrome browser
    And I have opened the rendered the payach template in the browser
    And I click on the show_ach_ui_button element
    And I set the sps.ach.billing_info.name_field element to the configured value ach.billing_info.name
    And I set the sps.ach.billing_info.address_field element to the configured value ach.billing_info.address
    And I set the sps.ach.billing_info.city_field element to the configured value ach.billing_info.city
    And I set the sps.ach.billing_info.state_field element to the configured value ach.billing_info.state
    And I set the sps.ach.billing_info.postal_code_field element to the configured value ach.billing_info.postal_code
    And I set the sps.ach.billing_info.country_field element to the configured value ach.billing_info.country
    And I set the sps.ach.checking_info.routing_number_field element to the configured value ach.routing_number
    And I set the sps.ach.checking_info.account_number_field element to the configured value ach.account_number
    And I set the sps.ach.checking_info.account_type_select element to the configured value ach.account_type
    And I click on the sps.ach.terms_and_conditions_checkbox element
    When I click on the sps.payment_submit_button element
    And I wait until the sps.result_div element is present
    Then the sps.approved_text element should exist

  @payjs2-63
  Scenario Outline: PAYJS2-63: ACH Modal missing text fields
    Given I have started a headless chrome browser
    And I have opened the rendered the payach template in the browser
    And I click on the show_ach_ui_button element
    And I set the sps.ach.billing_info.name_field element to the configured value ach.billing_info.name
    And I set the sps.ach.billing_info.address_field element to the configured value ach.billing_info.address
    And I set the sps.ach.billing_info.city_field element to the configured value ach.billing_info.city
    And I set the sps.ach.billing_info.state_field element to the configured value ach.billing_info.state
    And I set the sps.ach.billing_info.postal_code_field element to the configured value ach.billing_info.postal_code
    And I set the sps.ach.billing_info.country_field element to the configured value ach.billing_info.country
    And I set the sps.ach.checking_info.routing_number_field element to the configured value ach.routing_number
    And I set the sps.ach.checking_info.account_number_field element to the configured value ach.account_number
    And I set the sps.ach.checking_info.account_type_select element to the configured value ach.account_type
    And I click on the sps.ach.terms_and_conditions_checkbox element
    And I clear the <cleared_field> element
    And I wait 2 seconds
    Then the sps.payment_submit_button element should not be enabled

    Examples:
      | cleared_field                              |
      | sps.ach.billing_info.name_field            |
      | sps.ach.billing_info.address_field         |
      | sps.ach.billing_info.city_field            |
      | sps.ach.billing_info.state_field           |
      | sps.ach.billing_info.postal_code_field     |
      | sps.ach.billing_info.country_field         |
      | sps.ach.checking_info.routing_number_field |
      | sps.ach.checking_info.account_number_field |